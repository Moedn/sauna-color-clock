# Sauna Color Clock

**C code to use light colors as a sauna timer** 

You will need:

- a microcontroller able to run this code (e.g. ESP32 or Arduino UNO)
- an LED matrix that can be controlled by the FastLED library (e.g. WS2812, see [FastLED Wiki for reference](https://github.com/FastLED/FastLED/wiki/Chipset-reference))
- a potentiometer to set the time interval for the sauna timer (e.g. xxx); you can also opt out this part of the code if you want to work with a fix value (e.g. 15min)

ToDo before usage

- check whether the defined values work for your setup; default:
	+ LED data pin = 2
	+ potentiometer pin = 1
	+ number of LEDs = 64
