#include "FastLED.h"

/*
ToDo

- connect pot & read value → get min. and max. value → settle calculation below
*/

// set values

#define DATA_PIN 2
#define NUM_LEDS 64

CRGB leds[NUM_LEDS]; //block of memory that will be used for storing and manipulating the led data (`leds` array)

uint16_t hue = 0; //colour
const int pot_pin = 1; //

const int sauna_t_min = 10; //min Sauna time = 10min
const int sauna_t_max = 30; //max Sauna time = 30min

const int pol_val_min = 0; //min potentiometer value
const int pol_val_max = 1023; //max potentiometer value

int pot_val = analogRead(pot_pin); //potentiometer value

uint32_t hue_delay = ((sauna_t_max-sauna_t_min)/(pol_val_max-pol_val_min)*pot_val+sauna_t_min)*60*1000/256; //delay  (in milliseconds) until hue is incremented by 1

void setup() {

  Serial.begin(9600);

//LED Setup
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS).setCorrection(TypicalSMD5050); //load LED data
  FastLED.setBrightness(255); //set global brightness, applied regardless of what color(s) are shown on the LEDs

//  leds[0] = CRGB::White; FastLED.show(); //LED test

  for(int i = 0; i<NUM_LEDS; i++){ 
  leds[i].setHue(hue);
  }
  FastLED.show();

// print (mainly for debugging)
  Serial.println("potentiometer value is");
  Serial.print(pot_val);
  Serial.println(" ");
  Serial.println("which equals a delay of");
  Serial.print (hue_delay);
  Serial.println(" ");
  Serial.println("current hue is");
  Serial.print(hue);
  Serial.println(" ");Serial.println(" ");

  delay(hue_delay);
}

void loop() {
  pot_val = analogRead(pot_pin)
  hue_delay = ((sauna_t_max-sauna_t_min)/(pol_val_max-pol_val_min)*pot_val+sauna_t_min)*60*1000/256;
  
  hue++; // increment LED hue after hue_delay milliseconds
  
  for(int i = 0; i<NUM_LEDS; i++){ 
  leds[i].setHue(hue);
  }
  FastLED.show();

  delay(hue_delay);
}
